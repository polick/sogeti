package sogeti.techassessment.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sogeti.techassessment.Entities.Orders;
import sogeti.techassessment.Enums.OrderStatus;
import sogeti.techassessment.Services.OrderService;

import java.util.List;

@RestController
public class OrderController {

	@Autowired
	private OrderService service;

	@GetMapping("/orders/list")
	List<Orders> all() {
		return service.findAll();
	}

	@GetMapping("/orders/{id}")
	Orders getOrder(@PathVariable Long id) {
		return service.getById(id);
	}

	@GetMapping("/orders/customer/{id}")
	List<Orders> getOrderForCustomer(@PathVariable Long id) {
		return service.findByCustomer(id);
	}

	@PostMapping("/orders/create")
	void create(@RequestParam("customer") long customerNumber) {
		service.createNewOrder(customerNumber);
	}

	@PostMapping("/orders/update")
	HttpStatus update(@RequestParam("customer") long customerNumber, @RequestParam("orderId") long orderId, @RequestParam("orderStatus") int status) {
		if (service.updateOrder(customerNumber, orderId, OrderStatus.fromValue(status))) {
			return HttpStatus.ACCEPTED;
		}
		return HttpStatus.BAD_REQUEST;
	}

	@PostMapping("/orders/cancel")
	HttpStatus cancel(@RequestParam("customer") long customerNumber, @RequestParam("orderId") long orderId) {
		if (service.cancelOrder(orderId, customerNumber)) {
			return HttpStatus.ACCEPTED;
		}
		return HttpStatus.BAD_REQUEST;
	}
}


