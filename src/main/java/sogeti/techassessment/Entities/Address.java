package sogeti.techassessment.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Address {

	private @Id @GeneratedValue Long id;
	String attention;
	String houseNumber;
	String street;
	String city;
	String state;
	String postalCode;
}
