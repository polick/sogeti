package sogeti.techassessment.Entities;

import org.springframework.data.annotation.Id;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

public class Customer {
	private @Id Long id;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private @OneToOne Address address;

	private @OneToMany(fetch = FetchType.EAGER) List<sogeti.techassessment.Entities.Orders> Orders;
}
