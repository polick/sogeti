package sogeti.techassessment.Entities;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;
import sogeti.techassessment.Enums.OrderStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.IOException;

@Entity
@JsonComponent
@Table(name = "orders")
@NamedQuery(name = "Orders.findOne",
		query = "select o from Orders o where o.customerId = ?1 and o.id = ?2")
public class Orders {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;
	@Column(name = "status") OrderStatus status;
	@Column(name = "customer" ) Long customerId;

	public Long getId() {

		return id;
	}

	public OrderStatus getStatus() {

		return status;
	}

	public Long getCustomerId() {

		return customerId;
	}

	public Orders setId(Long id) {

		this.id = id;
		return this;
	}

	public Orders setCustomerId(Long customerId) {
		this.customerId = customerId;
		return this;
	}

	public Orders setStatus(OrderStatus status) {
		this.status = status;
		return this;
	}

	public static class Serializer extends JsonSerializer<Orders> {

		@Override
		public void serialize(Orders orders, JsonGenerator jsonGenerator,
				SerializerProvider serializerProvider) throws IOException,
				JsonProcessingException {

			jsonGenerator.writeStartObject();
			jsonGenerator.writeStringField(
					"Order Number",
					orders.getId().toString());
			jsonGenerator.writeStringField(
					"Customer",
					orders.getCustomerId().toString());
			jsonGenerator.writeStringField(
					"OrderStatus",
					orders.getStatus().name());
			jsonGenerator.writeEndObject();
		}
	}

}
