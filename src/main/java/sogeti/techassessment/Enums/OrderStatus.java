package sogeti.techassessment.Enums;

public enum OrderStatus {
	PENDING(0),
	PROCESSING(1),
	SHIPPED(2),
	CANCELLED(3);

	public final int status;
	OrderStatus(int status) {
		this.status = status;
	}

	public static OrderStatus fromValue(int status) {
		for (OrderStatus os : OrderStatus.values()){
			if (os.status == status){
				return os;
			}
		}
		return null;
	}
}
