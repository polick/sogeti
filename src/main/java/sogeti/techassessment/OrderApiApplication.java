package sogeti.techassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("sogeti.techassessment.Repositories")
@ComponentScan("sogeti.techassessment.Services")
@ComponentScan("sogeti.techassessment.Controllers")

@SpringBootApplication

public class OrderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderApiApplication.class, args);
	}

}
