package sogeti.techassessment.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import sogeti.techassessment.Entities.Orders;

import java.util.List;

@EnableJpaRepositories
public interface OrderRepository extends JpaRepository<Orders, Long> {

	List<Orders> findByCustomerId(Long customerId);
	Orders findOne(Long customerId, Long id);
}
