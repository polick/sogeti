package sogeti.techassessment.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sogeti.techassessment.Entities.Orders;
import sogeti.techassessment.Enums.OrderStatus;
import sogeti.techassessment.Repositories.OrderRepository;

import java.util.List;

@Service
public class OrderService {

	@Autowired
	private OrderRepository repository;

	public boolean cancelOrder(Long orderId, Long customerNumber) {

		Orders order = repository.findOne(customerNumber, orderId);

		if (order != null && (OrderStatus.PENDING.equals(order.getStatus()) || OrderStatus.PROCESSING.equals(order.getStatus()))) {
			order.setStatus(OrderStatus.CANCELLED);
			repository.save(order);
			return true;
		}

		return false;
	}

	public List<Orders> findAll() {
		return repository.findAll();
	}

	public List<Orders> findByCustomer (Long customerId) {
		return repository.findByCustomerId(customerId);
	}

	public void createNewOrder(Long customerNumber) {
		repository.save(new Orders().setCustomerId(customerNumber));
	}

	public Orders getById(Long id) {
		return repository.getById(id);
	}

	public boolean updateOrder(long customerNumber, long orderId, OrderStatus status) {

		Orders orders = getById(orderId);
		if (orders.getCustomerId().compareTo(customerNumber) == 0) {
			orders = orders.setStatus(status);
			repository.save(orders);
			return true;
		}

		return false;
	}
}
