package sogeti.techassessment.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sogeti.techassessment.Entities.Orders;
import sogeti.techassessment.Enums.OrderStatus;
import sogeti.techassessment.Repositories.OrderRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@WebMvcTest (OrderController.class)
class OrderControllerTest {

	@Autowired
	MockMvc mockMvc;
	@Autowired
	ObjectMapper mapper;

	@MockBean
	OrderRepository orderRepository;

	Orders ORDER_1 = new Orders().setId(1L).setStatus(OrderStatus.PROCESSING).setCustomerId(10L);
	Orders ORDER_2 = new Orders().setId(2L).setStatus(OrderStatus.PENDING).setCustomerId(9L);
	Orders ORDER_3 = new Orders().setId(3L).setStatus(OrderStatus.PROCESSING).setCustomerId(10L);
	Orders ORDER_4 = new Orders().setId(4L).setStatus(OrderStatus.CANCELLED).setCustomerId(9L);

	@Test
	void getOrders() throws Exception {
		List<Orders> records = new ArrayList<>(Arrays.asList(ORDER_1, ORDER_2, ORDER_3, ORDER_4));

		Mockito.when(orderRepository.findAll()).thenReturn(records);
		mockMvc.perform(MockMvcRequestBuilders
				.get("/orders")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(4)))
				.andExpect(jsonPath("$[2].Customer", is("10")))
				.andExpect(jsonPath("$[3].OrderStatus", is("CANCELLED")));
	}

	@Test
	void cancelOrdersShouldFailForNonPendingOrProcessing() throws Exception {

		Mockito.when(orderRepository.findOne(Mockito.anyLong(), Mockito.anyLong())).thenReturn(ORDER_4);
		mockMvc.perform(MockMvcRequestBuilders
				.post("/orders/cancel").param( "customer", "9").param("orderId" ,"4"))
				.andExpect(content().string("\"BAD_REQUEST\""));

	}

	@Test
	void cancelOrdersWhenOrderPendingOrProcessing() throws Exception {

		Mockito.when(orderRepository.findOne(Mockito.anyLong(), Mockito.anyLong())).thenReturn(ORDER_3);
		mockMvc.perform(MockMvcRequestBuilders
				.post("/orders/cancel").param( "customer", "10").param("orderId" ,"3"))
				.andExpect(content().string("\"ACCEPTED\""));

	}


}