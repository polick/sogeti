package sogeti.techassessment.Controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import sogeti.techassessment.Enums.OrderStatus;

@SpringBootTest
@TestPropertySource("classpath:application.properties")
public class OrderServiceTest {

	@Autowired
	OrderController orderController;

	@Test
	void getCustomerOrders() throws Exception {

		Assertions.assertEquals(orderController.getOrderForCustomer(10L).size(), 2);
	}

	@Disabled("Session Error")
	@Test
	void updateCustomerOrders() throws Exception {

		orderController.getOrder(2L);
		orderController.update(10L, 2L, 3);
		Assertions.assertEquals(orderController.getOrder(2L).getStatus(), OrderStatus.CANCELLED);

	}
}
